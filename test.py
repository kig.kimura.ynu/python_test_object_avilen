"""
このファイルに解答コードを書いてください
"""
path = "input.txt"

# ファイルの読み込み
with open(path) as f:
    x = [s.strip() for s in f.readlines()]

# xの最後の要素からmを取得
m = int(x[-1])
    
# ペアの辞書を作成
pairs = {}
for i in range(len(x) - 1):
    # 文字列を分割
    pair = x[i].split(":")
    pairs[pair[1]] = int(pair[0])
    
sorted_pairs = sorted(pairs.items(), key=lambda x:x[1])

# 出力の作成
out = []

# mを割った余りが0の場合，出力に追加
for i in sorted_pairs:
    if(m % i[1]==0):
        out.append(i[0])
        
# outに出力する文字列がない場合はmを出力
if(len(out)==0):
    print(m)
else:
    print("".join(out))